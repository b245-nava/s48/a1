// console.log('addPost');

let posts = [];
//  posts will serve as our mock database.
	// Formatted as an array of objects
		/*
			{
				id: value,
				title: value,
				body: value
			}
		*/

let count = 1;
	// Adding post data to the mock database
	document.querySelector('#form-add-post').addEventListener('submit', (event) => {
		// .preventDefault() stops the auto-reload of the webpage when the form is submitted
		event.preventDefault();

		let newPost = {
			id: count,
			title: document.querySelector('#txt-title').value,
			body: document.querySelector('#txt-body').value
		};

		console.log(newPost);

		// To save the new post in the mock database
		posts.push(newPost);
		console.log(posts);

		// To have auto-increment with our ObjectId
		count++;
		// Placing this method here will show the posts every time the submit event is triggered
		showPosts(posts);

		document.querySelector('#txt-title').value = "";
		document.querySelector('#txt-body').value = "";
	});

// [Section] Function to show posts

		const showPosts = (posts) => {
			
			// posts.reverse()
			let postEntries = ``;

			// This "posts" targets whatever is placed in the parameter of the function.
			posts.forEach((post) => {
				// Recall: += adds the value of the right operand to a variable
				postEntries += `
					<div id = "post-${post.id}">
						<h3 id = "post-title-${post.id}">${post.title}</h3>
						<p id = "post-body-${post.id}">${post.body}</p>
						<button onclick = "editPost(${post.id})">Edit</button>
						<button onclick = "deletePost(${post.id})">Delete</button>
					</div>

				`
			});

			document.querySelector('#div-post-entries').innerHTML = postEntries;
		}

// Logic: You can click the edit button of a post to auto-fill that in the update fields, then when you click update, that's what will show in the posts
// The edit button needs to send the post id, title, and body to the edit fields

// [Section] Edit Post

		const editPost = (id) => {
			// In this case, .innerHTML allows us to target the contents between the HTML tags indicated in the querySelector
			let title = document.querySelector(`#post-title-${id}`).innerHTML;
			let body = document.querySelector(`#post-body-${id}`).innerHTML;

			console.log(title);
			console.log(body);

			document.querySelector('#txt-edit-id').value = id;

			document.querySelector('#txt-edit-title').value = title;

			document.querySelector('#txt-edit-body').value = body;
		}

		document.querySelector('#form-edit-post').addEventListener("submit", (event) => {

			event.preventDefault();

			// Using .forEach() to check/find the document to be edited then using if/else

			posts.forEach(post => {
				let idToBeEdited = document.querySelector("#txt-edit-id").value;

				console.log(typeof idToBeEdited); // returns string, since the document.querySelector returns string
				console.log(typeof post.id); // returns number;
				if(post.id == idToBeEdited){

					// Remember: use .value to be able to target 
					let title = document.querySelector('#txt-edit-title').value;
					let body = document.querySelector('#txt-edit-body').value;

					posts[post.id-1].title = title;
					posts[post.id-1].body = body;

					alert("Edit is successful!");
					showPosts(posts);
					document.querySelector('#txt-edit-id').value = "";
					document.querySelector('#txt-edit-title').value = "";
					document.querySelector('#txt-edit-body').value = "";
				}
			});

		});

// [Section] Activity: Delete Button

	// The ID is obtained from the delete button above
	const deletePost = (id) => {
        const index = posts.findIndex((post) => post.id === id);
        if (index !== -1) {
          posts.splice(index, 1);
        }
        showPosts(posts);
      };